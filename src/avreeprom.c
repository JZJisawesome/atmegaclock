#include "avreeprom.h" 

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t AVREEPROM_read(uint16_t address)
{
    while (EECR & (1 << 1));//Wait for a previous write to finish (EEPE to clear)
    
    EEARH = (uint8_t)(address >> 8);//MSBs of address
    EEARL = (uint8_t)(address & 0xFF);//LSBs of address
    
    EECR |= 1;//Blocks until read finishes
    
    return EEDR;//Return data at address
}

uint8_t AVREEPROM_swap(uint8_t data, uint16_t address)//Returns old data at address
{
    cli();//Early to avoid any problems because of interrupts
    
    uint8_t oldData = AVREEPROM_read(address);//Will block while a previous write is in progress
    
    if (data != oldData)//Save write wear
    {
        //AVREEPROM_read already set the EEARH and EEARL registers, so we can skip that here
        EEARH = (uint8_t)(address >> 8);//MSBs of address
        EEARL = (uint8_t)(address & 0xFF);//LSBs of address
        
        EEDR = data;
        
        EECR |= 1 << 2;//Set EEMPE (start of timed sequence)
        EECR |= 1 << 1;//Set EEPE (end of timed sequence)
    }
    
    sei();
    return oldData;
}
